# DNA Mutation Service (DNAMS)

Este servicio está diseñado para trabajar con cualquier proveedor de servicio en la nube. Para esta documentación únicamente describiremos los pasos para ejecutar y desplegar el servicio desde GCP.

# Complejidad algorítmica (Big O)
El algoritmo usa un doble for por cada elemento de la matriz, con base en esto se analizó siguiendo los criterios de mejor y peor en términos de tiempo de ejecución. 
* En el **mejor** de los casos el algoritmo se dentendrá antes de recorrer la matriz, si detecta cualquier error durante la ejecución del **reshape**.
* En el **peor** de los casos el algoritmo se dentendrá antes de recorrer la matriz si detecta cualquier error en la creación de la misma.
* Debe completar el recorrido por cada fila **(n)** y después por cada columna **(n)**
  * Por lo que el orden de la búsqueda de mutaciones en la matriz normal es de **n^2**
  * Después de generar la matriz transpuesta vuelve a hacer de nuevo el recorrido **n^2**
* Para terminar con una operación de orden: **n^2 + n^2**
* Al reducir la operación nos queda que el orden del algoritmo para este proyecto es: **n^2**

# Seguridad

Todas las credenciales se encuentran en variables ocultas y no se hace referencia a ellas dentro del código. Asi mismo se usaron dos cuentas de servicio separadas una para el acceso a la base de datos por parte del proyecto de la app y una para permitir el CI/CD desde Gitlab.

# Diseño

* **Automatización.** La automatización del aprovisionamiento y las implementaciones de esta app aumenta la coherencia y la velocidad, y minimiza los errores humanos.

* **Acoplamiento flexible.** Este desarrollo contiene una colección de componentes independientes y de acoplamiento flexible. Que permite la independencia en como se distribuye a nivel físico los recursos, cómo la arquitectura y el diseño del almacenamiento de la información.

* **Diseño basado en datos.** Este desarrollo recopila métricas para comprender el comportamiento de su uso. Esto facilita la toma de decisiones sobre cuándo escalar el servicio o si no se encuentra en buen estado.

## Comenzando 🚀

Requieres de una cuenta de Google para poder usar el servicio de Google Console y asociar una cuenta de pago para el proyecto. 
Esta es la base para usar las herramientas de gservices y poder comenzar con la ejecución. Puedes iniciar el proceso desde aquí:

* [Google Console](https://console.cloud.google.com/?hl=es-419) - Google Console

Te aconsejamos informarte sobre el plan gratuito de Google antes de comenzar:

* [Start running workloads for free](https://cloud.google.com/free) - Free Tier GServices

Cuando hayas terminado con los pasos anteriores deberás crear un nuevo proyecto, para poder compilar y desplegar el servicio. 
En esta liga podrás encontrar información de como poder crear un nuevo proyecto:

* [Create new proyect](https://cloud.google.com/resource-manager/docs/creating-managing-projects) - Free Tier GServices

Mira **Deployment** para conocer como desplegar el proyecto.


### Pre-requisitos 📋

Requieres de tener una [Terminal de Google](https://cloud.google.com/shell/docs/using-cloud-shell#:~:text=Click%20the%20Activate%20Cloud%20Shell,the%20session%20to%20be%20initialized.) abierta para poder ejecutar los comandos e iniciar con el ambiente.

1. Primero deberás actualizar la información de los paquetes de configuración del sistema con:
```
sudo apt update
```
2. Habilitar el servicio de [Firestore](https://cloud.google.com/firestore) y [Cloud Run](https://cloud.google.com/run/docs/setup) desde la interfaz de GCP.
3. Crear una [colección](https://cloud.google.com/firestore/docs/quickstart-servers#create_a_in_native_mode_database) para usarlo más tarde
4. Crear las [credenciales](https://firebase.google.com/support/guides/service-accounts) de acceso al servicio
5. Agregar las [credenciales](https://cloud.google.com/firestore/docs/quickstart-servers#linux-or-macos) como variable de entorno en el path del sistema para otorgar acceso al servicio

### Instalación 🔧

Deberás instalar los siguientes paquetes introduciendo cada comando en la consola:
```
sudo apt install python3 python3-dev python3-venv
sudo apt install wget
wget https://bootstrap.pypa.io/get-pip.py
sudo python3 get-pip.py
```

Una vez terminando con la configuración anterior iniciaremos con la descarga de los archivos del repositorio. Ejecuta el siguiente comando en la terminal:
```
git clone https://gitlab.com/YOZTiK/dna-mutation.git
```

Esto te creara una carpeta y podrás iniciar con el despliegue o las pruebas locales.

## Ejecutando las pruebas en servidor local ⚙️

_Ahora ejecutaremos los siguientes comandos para tener un entorno de desarrollo ejecutandose desde la consola._
Tendremos que ingresar a la carpeta que se creó tras descargar el repositorio.
```
cd sso-service
```

_Si queremos iniciar el servicio de forma local deberemos ejecutar:_

```
python3 -m venv venv
source venv/bin/activate
```
_Una vez hayamos ejecutado los comandos anteriores para generar el ambiente de ejecución deberemos instalar los requisitos para el servicio:_
```
sudo pip install -r requirements.txt
```

_Por último bastará con ejecutar:_
```
sudo python3 app.py
```
_Esto hará que nuestro servicio se ejecute en localhost. Y nos mostrará el siguiente mensaje en la consola:_
```
 * Running on http://0.0.0.0:8080/ (Press CTRL+C to quit)
```

_Deberemos abrir otra pestaña de la consola y tendremos que enviar las peticiones ejecutando el siguiente comando para probar el servicio._
```
curl --header "Content-Type: application/json" --request POST --data '{<json-data>}' http://0.0.0.0:8080/<function-path>
```

Mira [Service Documentation](https://documenter.getpostman.com/view/10987523/TVetcRcV) para conocer como hacer las peticiones al servicio.

## Despliegue desde GCP 📦

Una vez teniendo el repositorio en la carpeta del proyecto "sso-service" bastará con ejecutar los siguientes comandos en la consola:

 ```
gcloud builds submit --tag gcr.io/<project-id>/<service-name>
```
_Para encontrar el project ID deberás ingresar a la información de de tu proyecto desde el dashboard de google console.
Mira [GService Documentation](https://cloud.google.com/resource-manager/docs/creating-managing-projects#identifying_projects) para conocer en dónde encontrar esta información._

Puede que tras ejecutar el comando anterior se te muestre un mensaje como este:

![gAuth permission](https://lh5.googleusercontent.com/pcQr7q-YanHZVA7-kBJu8W-Yk0A51dMQvLvFbPtdM8_N2nL0lAXUZr8hVhOqGUxi-bMzqVGHA76rjmlWyLmj=w1600-h756-rw)

O que en la consola se te muestre algo como:
```
API [cloudbuild.googleapis.com] not enabled on project [<#Project>].
 Would you like to enable and retry (this will take a few minutes)?
(y/N)?
```
Dependiendo del caso presiona Autorizar o ingresa "y" para continuar.

Como paso opcional puedes iniciar desde la consola el contenedor antes del deploy para hacer cualquier comprobación rápida. Solo tendrás que ejecutar el comando:
```
docker run --rm -env PORT=8080 -it -p 8080:8080 gcr.io/<project-id>/<service-name>
```

Después de haber ejecutado el comando anterior deberás iniciar con el despliegue ejecutando:

```
gcloud run deploy <service-name> --image gcr.io/<project-id>/<service-name> --platform managed --region us-central1 --allow-unauthenticated
```

O si deseas cambiar la configuración de zona o nombre del servicio puedes ejecutar el siguiente comando:

```
gcloud run deploy --image gcr.io/<project-id>/<service-name> --platform managed
```

Tras ejecutar el comando anterior se te pedira que ingreses el nombre del servicio en la consola:

```
Service name (<service-name>): 
```

Puedes ingresar el mismo nombre de "dnaservice" o cambiarlo. Tras haber elegido un nombre presiona enter para continuar

Tras ejecutar el comando te parecerán algunas opciones en la consola. Te recomendamos elegir una dependiendo de la cercanía a la que te encuentres del centro de datos de Google:
```
Please specify a region:
 [1] asia-east1
 [2] asia-east2
 [3] asia-northeast1
 [4] asia-northeast2
 [5] asia-northeast3
 [6] asia-south1
 [7] asia-southeast1
 [8] asia-southeast2
 [9] australia-southeast1
 [10] europe-north1
 [11] europe-west1
 [12] europe-west2
 [13] europe-west3
 [14] europe-west4
 [15] europe-west6
 [16] northamerica-northeast1
 [17] southamerica-east1
 [18] us-central1
 [19] us-east1
 [20] us-east4
 [21] us-west1
 [22] cancel
Please enter your numeric choice: 
```

Si te encuentras en México probablemente te convenga elegir la opción "18". Debes ingresar la opción deseada y presionar enter para continuar

```
Please enter your numeric choice:  18
```

Tras finalizar esta configuración te deberá aparecer el siguiente mensaje:

```
Deploying container to Cloud Run service [<service-name>] in project [<project-id>] region [us-central1]
✓ Deploying... Done.                                                           
  ✓ Creating Revision...                    
  ✓ Routing traffic...
Done.
Service [<service-name>] revision [<revision-id>] has been deployed and is serving 100 percent of traffic.
Service URL: https://<service-url>
```

El Service URL es el que ocuparemos para realizar las pruebas end-to-end de la siguiente fase.

### Analice las pruebas end-to-end 🔩

_Las pruebas se analizaron para cada elemento de funcionalidad devolviendo resultados exitosos de conexión entre el resto de servicios_

Mira [Service Documentation](https://documenter.getpostman.com/view/10987523/TVetcRcV) para ejecutar pruebas al servicio.

**Los URL de las pruebas se deben modificar dependiendo de en donde se encuentre el servicio** 

### Pruebas de conectividad y estrés ⌨️

Velocidad y confiabilidad

La pruebas a los endpoints (_mutation_ o _stats_) se ejecutan enviando una petición por un método POST. Encapsulando los parametros dentro del body de un json para el caso de _mutation_ o una petición GET para el caso de _stats_ a un URL similar a este:
 
```
https://<api-url>/<function>
```

Ver [Postman Documentation](https://documenter.getpostman.com/view/7130614/TVzNKKz1)

**El URL puede cambiar dependiendo de en donde se encuentre el servicio**

### UNITTEST ⌨️

Para las pruebas basta con ejecutar el siguiente comando y ejecutara pruebas para comprobar la fiabilidad del algoritmo de busqueda de mutaciones. 

```
python pytest.py -v
```


## Construido con 🛠️

_Las herramientas usadas para crear este proyecto_

* [Pycharm](https://www.jetbrains.com/es-es/pycharm/) - El IDE utilizado
* [Python3](https://docs.python.org/3/) - El lenguaje usado
* [Flask](https://flask.palletsprojects.com/en/1.1.x/) - El framework usado para el despliegue web
* [Docker](https://docs.docker.com/) - Para mantener la portabilidad y despliegue del proyecto  
* [Git](https://git-scm.com/doc) - Usado para manejar el versionamiento

También puedes mirar la lista de todos los [requisitos](requirements.txt) de este proyecto.

## Autores ✒️

_Este proyecto fue creado desde sus inicios por:_

* **Yoltic Cervantes Galeana** - *Análisis, Codificación y Documentación* - [YOZTiK](https://github.com/YOZTiK)

También puedes mirar la lista de todos los [contribuyentes](https://github.com/YOZTiK/sso-service/contributors) quíenes han participado en este proyecto. 

## Más fuentes de consulta ✒️

* [GitLab CI/CD pipeline configuration](https://docs.gitlab.com/ee/ci/yaml/)
* [CI/CD Examples](https://docs.gitlab.com/ee/ci/examples/)
* [CI/CD with GitLab and Google Cloud Run](https://www.youtube.com/watch?v=ZGOHUDVdmx0)
* [Install a specified Docker image](https://cloud.google.com/sdk/docs/downloads-docker)
* [Design Patterns](http://www.uml.org.cn/c++/pdf/designpatterns.pdf)

---
⌨️ con ❤️ por [YOZTiK](https://gitlab.com/YOZTiK/dna-mutation.git)  😊  
