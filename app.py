import os
import sys
from random import randint
from flask import Flask, request, make_response
from flask import jsonify
import requests
import json
import numpy as np
import DataBase as Databases

app = Flask(__name__)
database = Databases.Database()
time = 5


def hasMutation(jsonstr):
    # Get new token
    dna = jsonstr.get('dna')

    print(dna)
    if (dna is None):
        dna_status = 403
        jsonRaw = {
            'response': "error",
            'details': "header name doesn't exist in json obj",
            'mimetype': 'application/json'
        }
    else:
        if (len(dna) != 0):
            response = dna_algorithm(dna)
            dna_status = 200
            jsonRaw = {
                'response': response,
                'status': dna_status,
                'mimetype': 'application/json'
            }
        else:
            dna_status = 403
            jsonRaw = {
                'response': "error",
                'details': "header is empty",
                'mimetype': 'application/json'
            }

    jsonResponse = app.response_class(
        response=json.dumps(jsonRaw),
        status=dna_status,
        mimetype='application/json'
    )
    print(jsonResponse)
    return jsonResponse


def dna_algorithm(json):
    print("DNA Algorithm\nInput:\n", json)
    # Vars
    mutations = 0
    hasMutationFlag = False

    dna_arr = json_to_array(json)

    mutations, hasMutationFlag = search_mutations(dna_arr, 0, False, 0)

    print("Final hasMutation: ", hasMutationFlag)
    print("Final Mutations detected: ", mutations)

    mutationobj = database.update_no_mutations("0", str(1)) if mutations == 0 else database.update_mutations("0", str(
        mutations))
    jsonMutation = {
        'response': "success",
        'hasMutation': hasMutationFlag,
        'nmutations': mutations
    }

    return jsonMutation


def json_to_array(dna):
    dna_json_list = []
    number_of_columns = 0
    number_of_rows = 0

    number_of_columns = len(dna[0])
    number_of_rows = len(dna)

    for l in dna:
        dna_json_list += l

    dna_json_list = [c for c in dna_json_list]
    return np.array(dna_json_list).reshape(number_of_rows, number_of_columns)


def search_mutations(dna_arr, mutations, hasMutationFlag, rec_flag):
    hasMutationFlag = hasMutationFlag
    mutations = mutations
    act_letter = ''
    prev_letter = ''
    count = 1

    for row in dna_arr:
        for l in row:
            act_letter = l
            if act_letter == prev_letter:
                count += 1
                if count == 4:
                    hasMutationFlag = True
                    mutations += 1
            else:
                count = 1
            prev_letter = act_letter
        count = 1

    if rec_flag == 0:
        mutations, hasMutationFlag = search_mutations(dna_arr.transpose(), mutations, hasMutationFlag, rec_flag+1)

    return mutations, hasMutationFlag


def getStats():
    mutationobj = database.get_statistics("0")
    print(mutationobj.count_mutation)
    print(mutationobj.count_no_mutation)
    dna_status = 200

    jsonRaw = {'status': dna_status,
               "count_mutations": mutationobj.count_mutation,
               "count_no_mutation": mutationobj.count_no_mutation,
               "ratio": 0
               } if mutationobj.count_no_mutation == "0" else {
        'status': dna_status,
        "count_mutations": mutationobj.count_mutation,
        "count_no_mutation": mutationobj.count_no_mutation,
        "ratio": int(mutationobj.count_mutation) / int(mutationobj.count_no_mutation)
    }

    jsonResponse = app.response_class(
        response=json.dumps(jsonRaw),
        status=dna_status,
        mimetype='application/json'
    )
    print(jsonResponse)
    return jsonResponse


@app.route('/mutation', methods=['POST'])
def has_mutations():
    try:
        jsonstr = request.get_json()
        return hasMutation(jsonstr)
    except BaseException as error:
        return ('An exception occurred: {}'.format(error), 403)


@app.route('/stats', methods=['GET'])
def get_stats():
    try:
        return getStats()
    except BaseException as error:
        return ('An exception occurred: {}'.format(error), 403)


@app.route('/')
def home():
    return "Welcome to DNA Service"