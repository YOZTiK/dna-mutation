import unittest
from app import json_to_array, search_mutations

class TestStringMethods(unittest.TestCase):

    def test_createarray(self):
        print("Create Array Test")
        jsonstr = ["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"]
        arr = json_to_array(jsonstr)
        self.assertEqual(arr.shape, (6, 6))

    def test_mutations(self):
        print("Test search mutation algorithm")
        mutations = 0
        hasMutationFlag = False

        jsonstr = ["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"]
        arr = json_to_array(jsonstr)

        mutations, hasMutationFlag = search_mutations(arr, 0, False, 0)

        self.assertTrue(hasMutationFlag)
        self.assertEqual(mutations, 2)

    def test_no_mutations(self):
        print("Test search mutation algorithm")
        mutations = 0
        hasMutationFlag = False

        dna_json_list = ['A', 'T', 'G', 'C', 'G', 'A',
                         'C', 'A', 'G', 'T', 'G', 'C',
                         'T', 'T', 'A', 'T', 'A', 'T',
                         'A', 'G', 'A', 'A', 'G', 'G',
                         'C', 'C', 'T', 'C', 'T', 'A',
                         'T', 'C', 'A', 'C', 'T', 'G']
        dna_arr = np.array(dna_json_list).reshape(6, 6)

        mutations, hasMutationFlag = search_mutations(dna_arr, 0, False, 0)

        self.assertFalse(hasMutationFlag)
        self.assertEqual(mutations, 0)

if __name__ == '__main__':
    unittest.main()