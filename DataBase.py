from google.cloud import firestore
import google.cloud.exceptions
from datetime import datetime, timedelta

class Statistic:

    # Get statistic of mutations
    # @param countMutation {number} - Count of Mutation cases
    # @param countNoMutation {number} - Count of no Mutation cases
    # @param {number} [company=0] - Company id
    def __init__(self, count_mutation, count_no_mutation, company=0):
        self.company = company
        self.count_mutation = count_mutation
        self.count_no_mutation = count_no_mutation

class Database(object):

    def __init__(self):
        self.mutation_db = firestore.Client().collection(u'mutations')

    # Create a new company
    # @param company {Object} - Company Object
    # @return {boolean} - Success
    def create_company(self, obj):
        doc_ref = self.mutation_db.document(obj.company)
        doc_ref.set({
            u'company': obj.company,
            u'countMutation': obj.count_mutation,
            u'countNoMutation': obj.count_no_mutation
        })
        return True

    # Upload the new statistics for a company
    # @param company {string} - Company id
    # @param countMutation {string} - Count of Mutation cases
    # @param countNoMutation {string} - Count of no Mutation cases
    # @return {boolean} - Success
    def update_statistics(self, company, count_mutation, count_no_mutation):
        doc_ref = self.mutation_db.document(company)
        doc_ref.update({
            u'countMutation': count_mutation,
            u'countNoMutation': count_no_mutation
        })
        return True

    # Upload the new mutation statistics for a company
    # @param company {string} - Company id
    # @param countMutation {string} - Count of Mutation cases
    # @return {boolean} - Success
    def update_mutations(self, company, count_mutation):
        doc_ref = self.mutation_db.document(company)

        try:
            cmpny_dic = doc_ref.get()

            if cmpny_dic.exists:
                cmpny_dic = cmpny_dic.to_dict()
                doc_ref.update({
                    u'countMutation': str(int(cmpny_dic["countMutation"]) + int(count_mutation))
                })
                return True
            else:
                return False
        except google.cloud.exceptions.NotFound:
            return False

    # Upload the new nop mutation statistics for a company
    # @param company {string} - Company id
    # @param countNoMutation {string} - Count of no Mutation cases
    # @return {boolean} - Success
    def update_no_mutations(self, company, count_no_mutation):
        doc_ref = self.mutation_db.document(company)

        try:
            cmpny_dic = doc_ref.get()

            if cmpny_dic.exists:
                cmpny_dic = cmpny_dic.to_dict()
                doc_ref.update({
                    u'countNoMutation': str(int(cmpny_dic["countNoMutation"]) + int(count_no_mutation))
                })
                return True
            else:
                return False
        except google.cloud.exceptions.NotFound:
            return False


    # Get Statistics of mutations
    # @param company {number} - Company id
    # @return {Object} - Company Object
    # @throws Will trows False if the company doesn't exist
    def get_statistics(self, company):
        doc_ref = self.mutation_db.document(company)

        try:
            cmpny_dic = doc_ref.get()

            if cmpny_dic.exists:
                cmpny_dic = cmpny_dic.to_dict()
                statistic = Statistic(cmpny_dic["countMutation"], cmpny_dic["countNoMutation"])
                return statistic
            else:
                return False               

        except google.cloud.exceptions.NotFound:
            return False


    # Remove Company
    # @param company {number} - Company id
    # @return {boolean} - Success
    def remove_company(self, company):
        self.mutation_db.document(company).delete()
        return True
